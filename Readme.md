Mindset and Exploratory Testing Exercise:
----------------------------------------
I have added my views and thought process in the text file which is uploaded under 

Atlassian.Core.Mindsetandexploratory.Exercises -> ExerciseDocs

Automation Framework:
--------------------
Langugage : C#

Framework development approach : BDD(Behaviour Driven Development) approach,Page Object Model

Technology & Tools : .Netcore,Specflow,Nunit,Specflow runner,Selenium Webdriver,Log4j

High level Work Flow:
--------------------
Feature File - > Step definitions -> Page Objects- > Driver Facade -> Webdriver

Challenges:
----------
I have used . Netcore to build this framework with Specflow and this approach gives little bit pain on fixing a few dependencies.