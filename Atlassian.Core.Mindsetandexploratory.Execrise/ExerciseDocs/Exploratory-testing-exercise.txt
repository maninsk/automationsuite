Exploratory Testing:
===================
Test Approach:
-------------
As I don't have defined test scenarios, So I have taken some different type of approach like Exploratory/Monkey/Randon testing.
which will help to explore this functionality. Also, I have used some test techniques to validate the feature.

Test scenarios:
--------------

1)Validate restriction button is visible and accessible
2)Validate the default option "Anyone can view and edit" is available
3)Validate once the restriction button is clicked it should pop up the options window in an appropriate size and with right content/language
4)Validate drop-down list is displayed with below options
    Anyone can view and edit
    Editing restricted,
    Viewing and editing restricted
5)Validate appropriate image displayed for each option in the drop-down list
6)Validate "Anyone can view and edit" are selectable and there should not be user list and ADD button enabled
7)Validate "Editing restricted" is selectable and it should allow the user to add user id/groups 
8)Validate "Viewing and editing restricted" is selectable and it should allow the user to add user id/groups 
9)Also, validate updated user having the right level of access for Anyone can view and edit, Editing restricted, Viewing and editing restricted
10)Validate Help, Apply and Cancel buttons are clickable
11)Validate after clicking Apply button user-id/groups are added in the restriction list
12)Validate applied restriction are working properly for a specific user.
11)Validate after clicking Help button, It should navigate to right link
12)Validate the content displayed according to the country prefered language.(Ex: if I access from AU, it should be in English)
13)Validate the performance of functionality with x no of users
14)Validate the behaviour in a different browser(Ex: Chrome, IE, Firefox etc...)
15)Validate the behaviour in a different environment(Ex: Desktop, Laptop, MObile devices etc...)


Issues:
------

1)I was not able to add any user/group in Editing restricted, Viewing and editing restricted
2)Add button is enabled, but not able to add any user/group
3)By default, my user id is enabled in both the options (Editing restricted, Viewing and editing restricted)

Because of the above issue, I couldn't test the below behaviour of the feature.
a)Validate user can be able to move from 
    Anyone can view and edit -> Editing restricted
    Anyone can view and edit -> Viewing and editing restricted
    Editing restricted -> Viewing and editing restricted
    Editing restricted -> Anyone can view and edit
    Viewing and editing restricted -> Editing restricted
    Viewing and editing restricted -> Anyone can view and edit


List of things which I look before promote any development code base to Production:
-----------------------------------------------------------------------------------
1)Code review has to be completed(pull request should have raised after the feature implementation)
2)All the Automation test(Unit, Component, Integration, E2E Acceptance test) should have passed in the pipeline.
3)Also, performance, Resilience and security related testing should have completed.
4)The developed feature should have demoed to all the stakeholders to give better visibility/get the feedback.
5)User acceptance test should have done if it is necessary
