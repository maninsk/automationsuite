﻿namespace Atlassian.core.web.acceptancetest.StepDefinition
{
    using NUnit.Framework;
    using TechTalk.SpecFlow;
    using Atlassian.core.web.acceptancetest.Helpers;

    [Binding]
    public class RestrictionOptionsSteps
    {
        private DriverFacade _driverFacade;
        private Pages _pages;
        private Logger _logger;
        
        public RestrictionOptionsSteps(Pages pages, DriverFacade driverFacade)
        {       
            _driverFacade = driverFacade;
            _pages = pages;
            _logger = new Logger();
        }
        
        [When(@"the user modify the restriction from (.*) to (.*)")]
        public void WhenTheUserModifyTheRestrictionEditingRestricted(string restrictionOption,string newRestrictionOption)
        {
            _pages.HomePage.ClickConfluence();
            _pages.HomePage.ClickRestriction();
            _pages.HomePage.ModifyTheRestrictionOption(newRestrictionOption);
        }
        
        [Then(@"the user permission should be changed to (.*)")]
        public void ThenTheUserPermissionShouldBeChnagedToEditingRestricted(string expectedRestrictionOption)
        {
            _pages.HomePage.ClickRestriction();
             var actualRestrictionDescription = _pages.HomePage.GetRestrictionDescription();
            Assert.AreEqual("expectedRestrictionOption", "actualRestrictionDescription", $"Actual restriction name '{actualRestrictionDescription}' not equals to'{expectedRestrictionOption}'");
        }
    }
}
