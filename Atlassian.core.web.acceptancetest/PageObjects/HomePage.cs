﻿namespace Atlassian.core.web.acceptancetest.PageObjects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;
    using SeleniumExtras.PageObjects;
    using Atlassian.core.web.acceptancetest.Helpers;

    public class HomePage 
    {
        private DriverFacade _driverFacade;

        private WebDriverWait wait;

        private IWebDriver driver;

       
        public HomePage(DriverFacade driverFacade) 
        {       
            _driverFacade = driverFacade;
        }

        [FindsBy(How = How.XPath, Using = "username")]
        protected IWebElement ConfluencElement { get; set; }

        [FindsBy(How = How.XPath, Using = "password")]
        protected IWebElement RestrictionElement { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='com - atlassian - confluence']/div[2]/div[5]/div/div[3]/div[2]/div/div/div/div/div/div[1]/div[2]/span")]
        protected IWebElement RestrictionDescription { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='confluence - ui']/div[3]/div/div[1]/div/div[1]/div[1]/div[3]/div[1]/div/a")]
        protected IWebElement HomeElement { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='com - atlassian - confluence']/div[2]/div[5]/div/div[3]/div[2]/div/div/div/div/div/div[1]/div[1]/div/div")]
        protected IWebElement RestrictionDropDownMenu { get; set; }

        [FindsBy(How = How.Id, Using = "Apply")]
        protected IWebElement ApplyButton { get; set; }

        [FindsBy(How = How.Id, Using = "Cancel")]
        protected IWebElement CancelButton { get; set; }

        public void ClickConfluence()
        {
            ConfluencElement.Click();
        }

        public void ClickRestriction()
        {
            RestrictionElement.Click();
        }

        public bool IsRestrictionDescriptionEnabled()
        {
            return RestrictionDescription.Displayed;
        }

        public string GetRestrictionDescription()
        {
            return RestrictionDescription.GetAttribute("name");
        }

        public void IsHomeTabEnabled()
        {
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            wait.Until(driver => !driver.FindElement(By.XPath("")).Displayed);
        }

        public void ModifyTheRestrictionOption(string restrictionName)
        {
            List<string> lstWindow = driver.WindowHandles.ToList();
            foreach (var handle in lstWindow)
            {
                if (handle.Equals(restrictionName))
                {
                    driver.SwitchTo().Window(handle);

                    //To do -- SelectElelment reference is not available from OpenQA.Selenium.Support.UI
                    //SelectElement select = new SelectElement(RestrictionDropDownMenu.Click());

                    //select.SelectByValue(restrictionName);                   
                }
            }
            ApplyButton.Click();
            driver.SwitchTo().Window(this.driver.WindowHandles.Last());
       }
    }
}
