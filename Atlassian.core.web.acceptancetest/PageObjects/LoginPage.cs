﻿namespace Atlassian.core.web.acceptancetest.PageObjects
{

    using OpenQA.Selenium;
    using SeleniumExtras.PageObjects;
    using Atlassian.core.web.acceptancetest.Helpers;
    using Atlassian.Core.Web.Acceptancetests.PageObjects;

    public class LoginPage : BasePage
    {
        private DriverFacade _driverFacade;
        private IWebDriver _driver;

        public LoginPage(DriverFacade driverFacade) :base(driverFacade)
        {
            _driverFacade = driverFacade;            
        }

        [FindsBy(How = How.Id, Using = "username")]
        protected IWebElement UsernameTxtBox { get; set; }

        [FindsBy(How = How.Id, Using = "password")]
        protected IWebElement PasswordTxtBox { get; set; }

        [FindsBy(How = How.Id, Using = "login-submit")]
        protected IWebElement LoginBtn { get; set; }

        public void TypeName(string name)
        {
            UsernameTxtBox.SendKeys(name);
        }

        public void TypePassword(string password)
        {
            PasswordTxtBox.SendKeys(password);
        }

        public void ClickLoginButton()
        {
            LoginBtn.Click();
        }
     
        public void Login(string name, string password)
        {
            TypeName(name);
            TypePassword(password);
            ClickLoginButton();
        }
    }
}
