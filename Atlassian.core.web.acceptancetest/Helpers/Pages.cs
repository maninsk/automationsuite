﻿namespace Atlassian.core.web.acceptancetest.Helpers
{
    using Atlassian.core.web.acceptancetest.PageObjects;
    
    public class Pages
    {
        public SignUpPage SignUpPage { get; set; }
        public  LoginPage LoginPage { get; set; }

        public HomePage HomePage { get; set; }
    }
}
