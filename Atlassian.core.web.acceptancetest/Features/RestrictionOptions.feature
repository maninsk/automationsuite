﻿Feature: RestrictionOptions validation

@mytag
Scenario Outline: The restriction should be modifyable for the registered user
	Given the user sucessfully logged in with valid <UserName> and <Password> 
	When the user modify the restriction from <RestrictionOption> to <NewRestrictionOption>
	Then the user permission should be changed to <NewRestrictionOption>
	
	Examples:
		| Username | Password | RestrictionOption        | NewRestrictionOption			   |
		| Test     | xxxxxxxx | Anyone can view and eidt | Editing restricted			   |
		| Test     | xxxxxxxx | Editing restricted       | Viewing and editing restricted  |
